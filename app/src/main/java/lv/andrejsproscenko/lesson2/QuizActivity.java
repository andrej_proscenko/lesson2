package lv.andrejsproscenko.lesson2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class QuizActivity extends HeaderActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_form);
    }

    public void onSubmitButtonClick(View view) {
        //TODO: implement some logic
        TextView firstName = findViewById(R.id.first_name);
        TextView lastName = findViewById(R.id.last_name);
        TextView age = findViewById(R.id.age);
        RadioGroup chosenDayRadioGroup = findViewById(R.id.daysRadioGroup);
        int radioButtonID = chosenDayRadioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = chosenDayRadioGroup.findViewById(radioButtonID);

    }
}
