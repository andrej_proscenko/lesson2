package lv.andrejsproscenko.lesson2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onLoginButtonClick(View view) {
        TextView emailInput = findViewById(R.id.email_input);
        TextView passwordInput = findViewById(R.id.password_input);

//        if(emailInput == null || passwordInput == null) {
//            return;
//        }
        startActivity(new Intent(this, QuizActivity.class));

    }
}
