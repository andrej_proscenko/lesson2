package lv.andrejsproscenko.lesson2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class HeaderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.header);

//        ImageView backButtonImg = findViewById(R.id.back_button);
//        backButtonImg.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        ImageView closeButtonImg = findViewById(R.id.close_button);
//        closeButtonImg.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                finish();
//            }
//        });
    }

    public void onCloseButtonClick (View view){
        finish();
    }

    public void onBackButtonClick (View view){
        finish();
    }

}
